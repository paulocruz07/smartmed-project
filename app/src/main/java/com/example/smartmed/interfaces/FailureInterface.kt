package com.example.smartmed.interfaces

import com.example.smartmed.db.entity.Medication

interface FailureInterface {
    fun onRetrofitFail(error: String)
    fun onRetrofitSuccess(result: List<Medication>)
}