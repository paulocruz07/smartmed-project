package com.example.smartmed.interfaces

import com.example.smartmed.db.entity.Medication

interface DeleteInterface {
    fun deleteItem(medication: Medication)
}