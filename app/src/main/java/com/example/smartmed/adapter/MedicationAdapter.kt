package com.example.smartmed.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmed.interfaces.DeleteInterface
import com.example.smartmed.db.entity.Medication
import com.example.smartmed.R

class MedicationAdapter(var deleteInterface: DeleteInterface) :
    RecyclerView.Adapter<MedicationAdapter.MedicationHolder>() {
    private var medications: List<Medication> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicationHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.medication_item, parent, false)
        return MedicationHolder(itemView)
    }

    override fun onBindViewHolder(holder: MedicationHolder, position: Int) {
        val currentMedication = medications[position]
        holder.textViewTitle.text = currentMedication.name
        holder.textViewDescription.text = "" + currentMedication.quantity
    }

    override fun getItemCount(): Int {
        return medications.size
    }

    fun setMedications(medications: List<Medication>) {
        this.medications = medications
        notifyDataSetChanged()
    }

    fun removeAt(_position: Int) {
        deleteInterface.deleteItem(this.medications[_position])
        notifyItemRemoved(_position)
    }


    inner class MedicationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewTitle: TextView =
            itemView.findViewById<TextView>(R.id.text_view_name) as TextView
        var textViewDescription: TextView =
            itemView.findViewById<TextView>(R.id.text_view_quantity) as TextView

    }
}