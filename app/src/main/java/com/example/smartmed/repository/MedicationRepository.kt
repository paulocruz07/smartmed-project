package com.example.smartmed.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.smartmed.db.MedicationDatabase
import com.example.smartmed.db.dao.MedicationDao
import com.example.smartmed.db.entity.Medication
import java.lang.Exception

class MedicationRepository(application: Application) {

    private var noteDao: MedicationDao

    private var allMedications: LiveData<List<Medication>>

    init {
        val database: MedicationDatabase = MedicationDatabase.getInstance(
            application.applicationContext
        )!!
        noteDao = database.medicationDao()
        allMedications = noteDao.getAllMedication()
    }

    fun insert(note: Medication) {
        InsertMedicationAsyncTask(
            noteDao
        ).execute(note)
    }

    fun insertAll(note: List<Medication>) {
        InsertAllMedicationAsyncTask(
            noteDao
        ).execute(note)
    }

    fun deleteMedication(note: Medication) {
        DeleteMedicationAsyncTask(
            noteDao
        ).execute(note)
    }

    fun getAllMedications(): LiveData<List<Medication>> {
        return allMedications
    }

    private class InsertMedicationAsyncTask(noteDao: MedicationDao) :
        AsyncTask<Medication, Unit, Unit>() {
        val noteDao = noteDao

        override fun doInBackground(vararg p0: Medication?) {
            noteDao.insert(p0[0]!!)
        }
    }

    private class InsertAllMedicationAsyncTask(noteDao: MedicationDao) :
        AsyncTask<List<Medication>, Unit, Unit>() {
        val noteDao = noteDao

        override fun doInBackground(vararg p0: List<Medication>?) {
            p0[0]!!.forEach {
                noteDao.insert(it)
            }
        }
    }


    private class DeleteMedicationAsyncTask(val noteDao: MedicationDao) :
        AsyncTask<Medication, Unit, Unit>() {

        override fun doInBackground(vararg p0: Medication?) {
            noteDao.deleteMedication(p0[0]!!)
        }
    }

}