package com.example.smartmed.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "medication_table")
data class Medication(

    @PrimaryKey
    var name: String,

    var quantity: Int

)