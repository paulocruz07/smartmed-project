package com.example.smartmed.db

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.smartmed.db.dao.MedicationDao
import com.example.smartmed.db.entity.Medication

@Database(entities = [Medication::class], version = 2)
abstract class MedicationDatabase : RoomDatabase() {

    abstract fun medicationDao(): MedicationDao


    companion object {
        private var instance: MedicationDatabase? = null

        fun getInstance(context: Context): MedicationDatabase {
            if (instance == null) {
                synchronized(MedicationDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MedicationDatabase::class.java, "medications_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDbAsyncTask(instance)
                    .execute()
            }
        }

    }

    class PopulateDbAsyncTask(db: MedicationDatabase?) : AsyncTask<Unit, Unit, Unit>() {
        private val medicationDao = db?.medicationDao()

        override fun doInBackground(vararg p0: Unit?) {
            medicationDao?.insert(Medication("Title 1", 1))
            medicationDao?.insert(Medication("Title 2", 2))
            medicationDao?.insert(Medication("Title 3", 3))
        }
    }

}