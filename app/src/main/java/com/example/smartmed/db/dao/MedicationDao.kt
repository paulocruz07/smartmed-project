package com.example.smartmed.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.smartmed.db.entity.Medication

@Dao
interface MedicationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(medication: Medication)

    @Query("DELETE FROM medication_table")
    fun deleteAllMedication()

    @Delete
    fun deleteMedication(medication: Medication)

    @Query("SELECT * FROM medication_table ORDER BY name ASC")
    fun getAllMedication(): LiveData<List<Medication>>

}