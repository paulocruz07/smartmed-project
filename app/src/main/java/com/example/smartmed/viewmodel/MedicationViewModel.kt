package com.example.smartmed.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.smartmed.R
import com.example.smartmed.adapter.MedicationAdapter
import com.example.smartmed.db.entity.Medication
import com.example.smartmed.interfaces.FailureInterface
import com.example.smartmed.network.ApiInterface
import com.example.smartmed.network.BaseViewModel
import com.example.smartmed.repository.MedicationRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.net.InterfaceAddress
import javax.inject.Inject

class MedicationViewModel(application: Application) :
    BaseViewModel(application) {

    private var repository: MedicationRepository = MedicationRepository(application)

    private var allMedications: LiveData<List<Medication>> = repository.getAllMedications()

    private lateinit var subscription: Disposable

    private lateinit var failureInterface: FailureInterface

    @Inject
    lateinit var postApi: ApiInterface

    fun insert(medication: Medication) {
        repository.insert(medication)
    }

    fun deleteMedication(medication: Medication) {
        repository.deleteMedication(medication)
    }

    fun insertAllMedication(medication: List<Medication>) {
        repository.insertAll(medication)
    }

    fun getAllMedications(): LiveData<List<Medication>> {
        return allMedications
    }


    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    fun loadMedicationsByService() {
        subscription = postApi.getMedications()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                // Add result
                { result -> onRetrievePostListSuccess(result) },
                { error -> onRetrievePostListError(error.toString()) }
            )
    }

    private fun onRetrievePostListSuccess(postList: List<Medication>) {
        failureInterface.onRetrofitSuccess(postList)
    }

    private fun onRetrievePostListError(error:String) {
        failureInterface.onRetrofitFail(error)
    }

    fun setFailureInterface(failureInterface: FailureInterface) {
        this.failureInterface = failureInterface
    }

}
