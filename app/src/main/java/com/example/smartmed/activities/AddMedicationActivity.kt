package com.example.smartmed.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.smartmed.R
import java.lang.Exception

class AddMedicationActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_NAME = "EXTRA_NAME"
        const val EXTRA_QUANTITY = "EXTRA_QUANTITY"
    }

    private lateinit var edit_text_name: EditText
    private lateinit var edit_text_quantity: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_medication)

        edit_text_name = findViewById<EditText>(R.id.edit_text_name)
        edit_text_quantity = findViewById<EditText>(R.id.edit_text_quantity)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_medication_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.save_note -> {
                saveNote()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun saveNote() {
        if (edit_text_name.text.toString().trim().isBlank() || edit_text_quantity.text.toString().trim().isBlank()) {
            Toast.makeText(this, "Can not insert empty note!", Toast.LENGTH_SHORT).show()
            return
        }

        val data = Intent().apply {
            putExtra(EXTRA_NAME, edit_text_name.text.toString())
            try {
                putExtra(EXTRA_QUANTITY, Integer.valueOf(edit_text_quantity.text.toString()))
            } catch (e: Exception) {
                putExtra(EXTRA_QUANTITY, -1)
            }
        }

        setResult(Activity.RESULT_OK, data)
        finish()
    }
}