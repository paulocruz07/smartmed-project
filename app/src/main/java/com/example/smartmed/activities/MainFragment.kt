package com.example.smartmed.activities

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartmed.*
import com.example.smartmed.utils.SwipeToDeleteCallback
import com.example.smartmed.adapter.MedicationAdapter
import com.example.smartmed.db.entity.Medication
import com.example.smartmed.interfaces.DeleteInterface
import com.example.smartmed.interfaces.FailureInterface
import com.example.smartmed.viewmodel.MedicationViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainFragment : Fragment(), DeleteInterface, FailureInterface {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val ADD_NOTE_REQUEST = 1

    private lateinit var viewModel: MedicationViewModel
    private val adapter = MedicationAdapter(this@MainFragment)
    private lateinit var recyclerView: RecyclerView
    private lateinit var buttonAddMedication: FloatingActionButton


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView = activity!!.findViewById(R.id.recycler_view)
        buttonAddMedication = activity!!.findViewById(R.id.buttonAddNote)

        initializeUI()

    }

    private fun initializeUI() {

        buttonAddMedication.setOnClickListener {
            startActivityForResult(
                Intent(activity, AddMedicationActivity::class.java),
                ADD_NOTE_REQUEST
            )
        }
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter

        val swipeHandler = object : SwipeToDeleteCallback(activity!!.applicationContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        viewModel = ViewModelProviders.of(this).get(MedicationViewModel::class.java)
        viewModel.setFailureInterface(this)

        getMedicationsByRetrofit()
    }

    private fun getMedicationsByRetrofit() {
        viewModel.loadMedicationsByService()
    }

    override fun onRetrofitSuccess(result: List<Medication>) {
        viewModel.insertAllMedication(result)
        getAllMedications()
    }

    private fun getAllMedications() {
        viewModel.getAllMedications().observe(viewLifecycleOwner,
            Observer<List<Medication>> { t -> adapter.setMedications(t!!) })
    }

    //Se o retrofit falhar, ficamos atentos ás mudanças na base de dados
    override fun onRetrofitFail(error: String) {
        Toast.makeText(
            activity,
            "Error getting from the server - displaying db information instead",
            Toast.LENGTH_SHORT
        ).show()
        getAllMedications()
    }

    override fun deleteItem(medication: Medication) {
        viewModel.deleteMedication(medication)
        Toast.makeText(activity, "Medication deleted!", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_NOTE_REQUEST && resultCode == Activity.RESULT_OK) {
            val newNote = Medication(
                data!!.getStringExtra(AddMedicationActivity.EXTRA_NAME),
                data.getIntExtra(AddMedicationActivity.EXTRA_QUANTITY, -1)
            )
            viewModel.insert(newNote)
            Toast.makeText(activity, "Medication saved!", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(activity, "Medication not saved!", Toast.LENGTH_SHORT).show()
        }
    }
}
