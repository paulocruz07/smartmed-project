package com.example.smartmed.network

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.smartmed.viewmodel.MedicationViewModel

abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MedicationViewModel -> injector.inject(this)
        }
    }
}