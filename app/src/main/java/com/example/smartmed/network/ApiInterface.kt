package com.example.smartmed.network

import com.example.smartmed.db.entity.Medication
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * The interface which provides methods to get result of webservices
 */
interface ApiInterface {
    /**
     * Get the list of the pots from the API
     */
    @GET("/medications")
    fun getMedications(): Observable<List<Medication>>
}