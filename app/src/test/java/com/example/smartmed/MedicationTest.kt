package com.example.smartmed

import com.example.smartmed.db.entity.Medication
import org.junit.Assert
import org.junit.Test

class MedicationTest {
    @Test
    fun creation_isCorrect() {
        val medicationMock = Medication("brufen", 200)
        Assert.assertEquals(medicationMock.name, "brufen")
        Assert.assertEquals(medicationMock.quantity, 200)
    }
}